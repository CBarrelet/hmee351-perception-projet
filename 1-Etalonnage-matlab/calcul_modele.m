% Etalonnage de la camera a partir du fichier uv_xyz
% Un modele de stenoppe classique est utilise
%
% Chargement de donnees 3D de la mire (cube) 
m=eval(['uv_xyz']);
n=max(size(m));

% Affectation des donnees du fichier aux varaiables
u=m(:,1);
v=m(:,2);
x=m(:,3);
y=m(:,4);
z=m(:,5);

% construction des matrices A et B
j = 1;
for i = 1:n 

    A(j,:)=[x(i),y(i),z(i),1,0,0,0,0,-u(i)*x(i),-u(i)*y(i),-u(i)*z(i)];
    B(j,1) = [u(i)];
    
    j = j+1;
    
    A(j,:)=[0,0,0,0,x(i),y(i),z(i),1,-v(i)*x(i),-v(i)*y(i),-v(i)*z(i)];
    B(j,1) = [v(i)];
    
    j = j+1;
end


% Impression de A et B pour verifier
A
B
% Calcul du mod?le avec la pseudo inverse
X=pinv(A)*B;

% Le modele complet
C=[X(1:4)'; X(5:8)'; X(9:11)',1]